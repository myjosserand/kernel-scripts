#! /usr/bin/env python
import argparse
import csv
import jinja2
import os
import sys
import re

from pygit2 import Repository, Tag
from pygit2 import GIT_SORT_TOPOLOGICAL

EMAIL_PATTERN = "@collabora.co"
CONTENT = " (?P<name>[\w\-\. ]+) <(?P<username>.*" + EMAIL_PATTERN + ".*)>"

contributors = {}
authored = {}
committed = {}
onbehalf = {}
signed = {}
reviewed = {}
acked = {}
tested = {}
reported = {}

OTHER_TAGS = [ 
    ("Reviewed-by:", reviewed),
    ("Acked-by:", acked),
    ("Tested-by:", tested),
    ("Reported-by:", reported)
]

def add_commit(dict, email, commit):
    if email not in dict.keys():
        dict[email] = [0, []]

    dict[email][0] += 1
    item = [str(commit.id), commit.message.split('\n')[0]]
    dict[email][1].append(item)

def add_contributor(contributors, email, name):
    if email not in contributors.keys():
        contributors[email] = name

def not_author_or_committer(commit, email, name):
    if commit.author.email != email and commit.committer.email != email:
        return True
    return False

def is_maintainer(commit, email, name):
    if (commit.author.email != email and commit.committer.email == email) \
    or (commit.author.name != name and commit.committer.name == name):
        return True
    return False

def total_commits(d):
    return sum((v[0] for v in d.values()))

def order_results(results, args):
    if args.numbered:
        return sorted(results.items(), key=lambda item: item[1], reverse=True)
    else:
        return sorted(results.items(), key=lambda item: contributors[item[0]])

def print_commits(f, results, args):
    for email, data in results:
        f.write("%s" % (contributors[email]))
        if args.email:
            f.write(" <%s>" % (email))
        f.write(" (%d):\n" % (data[0]))
        if not args.summary:
            for c in data[1]:
                f.write("\t%s %s\n" % (c[0][0:9], c[1]))
            f.write("\n")

def print_all(results, args):
    f = open(args.output, "w") if args.output else sys.stdout

    for r in results:
        f.write("{}\n\n".format(r[0]))
        print_commits(f, r[1], args)
        f.write("\n")

    if f is not sys.stdout:
        f.close()

def generate_html(results, args):
    env = jinja2.Environment(loader=jinja2.FileSystemLoader('{}/templates'.format(sys.path[0])))
    template = env.get_template('contribution-stats.html.j2')

    content = template.render(results=results, contributors=contributors, args=args)
    with open(args.html, 'w') as html_file:
        html_file.write(content)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.set_defaults(func=lambda x: parser.print_help())
    parser.add_argument('-s', '--summary', action='store_true',
                        help="sort output according to the number of commits instead of alphabetic order")
    parser.add_argument('-n', '--numbered', action='store_true',
                        help="suppress commit description and provide a commit count summary only")
    parser.add_argument('-e', '--email', action='store_true',
                        help="show the email address of each contributor")
    parser.add_argument('-o', '--output', metavar="FILE",
                        help="output to a file instead of stdout")
    parser.add_argument('--html', metavar="FILE",
                        help="output to a html instead of stdout")

    args = parser.parse_args(sys.argv[1:])

    repo = Repository(os.getcwd())
    for line in csv.reader(sys.stdin, delimiter=','):
        commit = repo.get(line[0])
        if EMAIL_PATTERN in commit.author.email:
            add_contributor(contributors, commit.author.email, commit.author.name)
            add_commit(authored, commit.author.email, commit)

        else:
            p = re.compile('Signed-off-by:' + CONTENT)
            match = p.findall(commit.message)
            for m in match:
                add_contributor(contributors, m[1], m[0])
                if not_author_or_committer(commit, m[1], m[0]):
                    add_commit(signed, m[1], commit)

                    add_contributor(contributors, commit.author.email, commit.author.name)
                    add_commit(onbehalf, commit.author.email, commit)

                if is_maintainer(commit, m[1], m[0]):
                    add_commit(committed, m[1], commit)

        for tag in OTHER_TAGS:
            p = re.compile(tag[0] + CONTENT)
            match = p.findall(commit.message)
            for m in match:
                add_contributor(contributors, m[1], m[0])
                add_commit(tag[1], m[1], commit)

    results = []
    results.append(("Authored ({}):".format(total_commits(authored)), order_results(authored, args)))
    results.append(("Maintainer Committed ({}):".format(total_commits(committed)), order_results(committed, args)))
    results.append(("Signed-off-by ({}):".format(total_commits(signed)), order_results(signed, args)))
    results.append(("On behalf of ({}):".format(total_commits(onbehalf)), order_results(onbehalf, args)))
    for tag in OTHER_TAGS:
        results.append(("{} ({}):".format(tag[0][0:-1], total_commits(tag[1])), order_results(tag[1], args)))

    if args.html:
        generate_html(results, args)
    else:
        print_all(results, args)
