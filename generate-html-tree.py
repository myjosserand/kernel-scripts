#! /usr/bin/env python
import pygit2 as git
import sys
import csv
import json
import os
from datetime import datetime
import jinja2
import argparse

#BASE_URL="https://chromium.googlesource.com/chromiumos/third_party/kernel/+/"
BASE_URL="https://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id="

def classify_commit(fs_dict, commit):
    diff = repo.diff(commit[0], commit[0] + "^")
    for delta in diff.deltas:
        path_l = delta.new_file.path.split('/')
        partial_dict = fs_dict
        for entry in path_l:
            if entry == path_l[-1]:
                if entry not in partial_dict.keys():
                    partial_dict[entry] = [[], 0]
                (partial_dict[entry][0]).append(commit)
            
            if entry not in partial_dict.keys():
                partial_dict[entry] = [{}, 0]

            partial_dict[entry][1] += 1
            partial_dict = partial_dict[entry][0]

def generate_html(repo, fs_dict, title, cmd):
    env = jinja2.Environment(loader=jinja2.FileSystemLoader('{}/templates'.format(sys.path[0])))
    template = env.get_template('backlog-tree.html.j2')

    return template.render(data=fs_dict, baseurl=BASE_URL, title=title, cmdline=cmd, now=datetime.now())

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.set_defaults(func=lambda x: parser.print_help())
    parser.add_argument('-t','--title', type=str, default="Kernel commits - tree view",
                        help="title for the HTML report")
    parser.add_argument('-c','--cmdline', type=str,
                        help="the command line used to generate the list of patches")
    parser.add_argument('-o', '--output', metavar="FILE", default="tree",
                        help="basename for the json and HTML output files")

    args = parser.parse_args(sys.argv[1:])

    repo = git.Repository(os.getcwd())
    fs_dict = {}

    for line in csv.reader(sys.stdin, delimiter=','):
        classify_commit(fs_dict, line)

    with open(args.output + '.json', 'w') as json_file:
        json.dump(fs_dict, json_file)

    html = generate_html(repo, fs_dict, args.title, args.cmdline)
    with open(args.output + '.html', 'w') as html_file:
        html_file.write(html)
