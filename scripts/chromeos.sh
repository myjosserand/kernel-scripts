#!/bin/bash

GIT_TREE="/home/gfpadovan/p/linux"
BRANCH="chromeos/chromeos-5.4"
WORKDIR=`pwd`
RANGE="v5.4.19..chromeos/chromeos-5.4"

pushd $GIT_TREE > /dev/null

NAME="chromeos-backlog-5.4"
HEAD=`git rev-parse $BRANCH`
CMDLINE="git log --no-merges --pretty='format:%h,"%s",,' $RANGE | egrep -v 'UPSTREAM|FROMGIT|FROMLIST|BACKPORT'"
eval $CMDLINE > $NAME.csv
mv $NAME.csv $WORKDIR

popd > /dev/null

source .venv/bin/activate
./generate-html-tree.py $NAME.csv $NAME "$CMDLINE" $HEAD                                                                                                                                                                                      
